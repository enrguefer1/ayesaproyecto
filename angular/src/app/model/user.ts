export class User {
  id: number;
  name: string;
  username: string;
  state: string;
  avatar_url: string;
  web_url: string;

  constructor(id: number, name: string, username: string, state: string, avatar_url: string, web_url: string) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.state = state;
    this.avatar_url = avatar_url;
    this.web_url = web_url;
  }
}

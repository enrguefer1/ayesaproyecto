import { getLocaleNumberSymbol } from '@angular/common';
import { User } from './user';

export class Issues {
  id: number;
  iid: number;
  title: string;
  state: string;
  description: string;
  confidential: boolean;
  assignees: Array<User>;
  milestone_id: number;
  labels: string;
  created_at: string;
  due_date: string;
  weight: number;


  constructor(id: number,
    iid: number,
    title: string,
    state?: string,
    description?: string,
    confidential?: boolean,
    assignees?: Array<User>,
    milestone_id?: number,
    labels?: string,
    created_at?: string,
    due_date?: string,
    weight?: number) {
      this.id = id;
      this.iid = iid;
      this.title = title;
      this.state = state;
      this.description = description;
      this.confidential = confidential;
      this.assignees = assignees;
      this.milestone_id = milestone_id;
      this.labels = labels;
      this.created_at = created_at;
      this.due_date = due_date;
      this.weight = weight;
  }
}

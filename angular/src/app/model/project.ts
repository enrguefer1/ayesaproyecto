import { User } from './user';

export class Project {
  id: number;
  iid: Array<number>;
  project_id: number;
  title: string;
  description: string;
  state: string;
  created_at: Date;
  updated_at: Date;
  closed_at: Date;
  closed_by: Date;
  labels: Array<string>;
  milestone: string;
  assignees: Array<User>;
  author: User;
  assignee: User;
  user_notes_count: number;
  upvotes: number;
  downvotes: number;
  due_date: number;
  confidential: number;
  discussion_locked: number;
  web_url: string;
  weight: number;

  constructor(id: number,
    iid: Array<number>,
    project_id: number,
    title: string,
    description: string,
    state: string,
    created_at: Date,
    updated_at: Date,
    closed_at: Date,
    closed_by: Date,
    labels: Array<string>,
    milestone: string,
    assignees: Array<User>,
    author: User,
    assignee: User,
    user_notes_count: number,
    upvotes: number,
    downvotes: number,
    due_date: number,
    confidential: number,
    discussion_locked: number,
    web_url: string,
    weight: number) {
      this.id = id;
      this.iid = iid;
      this.project_id = project_id;
      this.title = title;
      this.description = description;
      this.state = state;
      this.created_at = created_at;
      this.updated_at = updated_at;
      this.closed_at = closed_at;
      this.closed_by = closed_by;
      this.labels = labels;
      this.milestone = milestone;
      this.assignees = assignees;
      this.author = author;
      this.assignee = assignee;
      this.user_notes_count = user_notes_count;
      this.upvotes = upvotes;
      this.downvotes = downvotes;
      this.due_date = due_date;
      this.confidential = confidential;
      this.discussion_locked = discussion_locked;
      this.web_url = web_url;
      this.weight = weight;
  }
}

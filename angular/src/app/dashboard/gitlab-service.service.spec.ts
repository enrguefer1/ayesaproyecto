import { TestBed } from '@angular/core/testing';

import { GitlabServiceService } from './gitlab-service.service';

describe('GitlabServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GitlabServiceService = TestBed.get(GitlabServiceService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GitlabRestService {

  // httpOptions = {
  //   headers: new HttpHeaders({
  //     'PRIVATE-TOKEN': 'rhUwJEzrbs2_ZfNJ6QdC',
  //     'Content-Type':  'application/json'
  //   })
  // };

  constructor(private httpClient: HttpClient) { }


  getIssues() {
    return this.httpClient.get('https://gitlab.com/api/v4/projects/9958805/issues');
  }

  getIssueById(issueID: number) {
    return this.httpClient.get('https://gitlab.com/api/v4/projects/9958805/issues/' + issueID);
  }

  getMemberByName(username: string) {
     return this.httpClient.get('https://gitlab.com/api/v4/projects/9958805/members', {
       params: new HttpParams().set('query', username)
     });
  }

  CreateIssue(titulo: string, descripcion: string, ids: number) {
    return this.httpClient.post('https://gitlab.com/api/v4/projects/9958805/issues',
    {
      'title': titulo,
      'description': descripcion,
      'assignee_ids': ids
  });
}

  deleteIssue(iid: number): any {
    return this.httpClient.delete('https://gitlab.com/api/v4/projects/9958805/issues/' + iid.toString(),
    {observe: 'response'});
  }

  searchIssues(search: string) {
    return this.httpClient.get('https://gitlab.com/api/v4/projects/9958805/search', {
       params: new HttpParams().set('search', search)
       .set('scope', 'issues')
     });
  }

  getBoards() {
    return this.httpClient.get('https://gitlab.com/api/v4/projects/9958805/boards');
  }

  updateIssue(issue_iid: number, descripcion: string, titulo: string, ids: number) {
    return this.httpClient.put('https://gitlab.com/api/v4/projects/9958805/issues/' + issue_iid.toString(),
    {
      'title': titulo,
      'description': descripcion,
      'assignee_ids': ids
  });
  }

  closeIssue(issue_iid: number) {
    return this.httpClient.put('https://gitlab.com/api/v4/projects/9958805/issues/' + issue_iid.toString(),
    {
      'state_event': 'close'
  });
  }
}

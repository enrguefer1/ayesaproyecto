import { Component, OnInit } from '@angular/core';
import { Issues } from '../model/issues';
import { GitlabServiceService } from './gitlab-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  issuesOpen: Array<Issues> = [];
  issuesClosed: Array<Issues> = [];

  constructor(private service: GitlabServiceService) { }

  ngOnInit() {
    this.getIssues();
  }

  onItemDropChangeToClose(e: any) {
    console.log('Change to Close' + e);
    this.issuesClosed.push(e.dragData);
    this.issuesOpen = this.issuesOpen.filter(i => i.iid !== e.dragData.iid);
}

onItemDropChangeToOpen(e: any) {
  this.issuesOpen.push(e.dragData);
  this.issuesClosed = this.issuesClosed.filter(i => i.iid !== e.dragData.iid);
}

  getIssues() {
    this.service.getIssues().subscribe(data => {
      data.forEach(element => {
        const issue = new Issues(element.id, element.iid, element.title, element.state);
        if (issue.state === 'opened') {
          this.issuesOpen.push(issue);
        } else {
         this.issuesClosed.push(issue);
        }
      });
    });
  }

}

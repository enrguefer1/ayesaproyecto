import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { format } from 'util';
import { GitlabServiceService } from '../gitlab-service.service';
import { Issues } from '../../model/issues';

@Component({
  selector: 'app-listado-issue',
  templateUrl: './listado-issue.component.html',
  styleUrls: ['./listado-issue.component.css']
})
export class ListadoIssueComponent implements OnInit {


  issue: Issues;
  issues: Array<Issues> = [];
  search = '';

  constructor(private service: GitlabServiceService) { }

  ngOnInit() {
    this.getIssues();
  }

  onKey(event: any): void {
    if (this.search.length >= 3) {
      this.getIssuesBySearch();
    } else {
      this.getIssues();
    }
  }

  getIssues() {
    this.service.getIssues().subscribe(data => {
      this.issues = data;
    });
  }

  deleteIssue(issue: Issues) {
    this.service.deleteIssue(issue.iid).subscribe(resp => {
      if (resp.status === 204) {
        this.issues = this.issues.filter(i => i.iid !== issue.iid);
      }
  });
}

  getIssuesBySearch() {
    this.issues = [];
    this.service.searchIssues(this.search).subscribe(
      list => {
        this.issues = list;
  });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoIssueComponent } from './listado-issue.component';

describe('ListadoIssueComponent', () => {
  let component: ListadoIssueComponent;
  let fixture: ComponentFixture<ListadoIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleIssueComponent } from './detalle-issue.component';

describe('DetalleIssueComponent', () => {
  let component: DetalleIssueComponent;
  let fixture: ComponentFixture<DetalleIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

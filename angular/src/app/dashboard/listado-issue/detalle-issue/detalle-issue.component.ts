import { Component, OnInit, Input } from '@angular/core';
import { Issues } from 'src/app/model/issues';
import { ActivatedRoute } from '@angular/router';
import { GitlabServiceService } from '../../gitlab-service.service';

@Component({
  selector: 'app-detalle-issue',
  templateUrl: './detalle-issue.component.html',
  styleUrls: ['./detalle-issue.component.css']
})
export class DetalleIssueComponent implements OnInit {

  @Input() issue: Issues;
  idIssue: string;
  mostrarUpdate: boolean;

  constructor(private route: ActivatedRoute, private gitlabService: GitlabServiceService) {
    this.mostrarUpdate = false;
    this.idIssue = this.route.snapshot.paramMap.get('id');
    this.issue = this.gitlabService.getIssue(this.idIssue as unknown as number)
      .subscribe(arg => this.issue = arg);
      console.log(this.route.snapshot.url[0].path);

      if (this.route.snapshot.url[0].path === 'editIssue') {
        this.mostrarUpdate = true;
      }
   }


  ngOnInit() {
  }

  updateIssue() {
    this.gitlabService.updateIssue(this.issue.iid, this.issue.assignees[0].name, this.issue.description, this.issue.title);
    }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoIssueComponent } from './nuevo-issue.component';

describe('NuevoIssueComponent', () => {
  let component: NuevoIssueComponent;
  let fixture: ComponentFixture<NuevoIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

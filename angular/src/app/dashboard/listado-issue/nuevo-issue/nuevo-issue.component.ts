import { Component, OnInit, Input } from '@angular/core';
import { Issues } from 'src/app/model/issues';
import { GitlabServiceService } from '../../gitlab-service.service';

@Component({
  selector: 'app-nuevo-issue',
  templateUrl: './nuevo-issue.component.html',
  styleUrls: ['./nuevo-issue.component.css']
})
export class NuevoIssueComponent implements OnInit {

  @Input() issue: Issues;
  usuario: string;
  title: string;
  description: string;

  constructor(private service: GitlabServiceService) { }

  ngOnInit() {
  }

  addIssue() {
    this.service.creatIssue(this.usuario, this.description, this.title);
  }

  addContinueIssue() {
    this.service.creatIssue(this.usuario, this.description, this.title);
    this.usuario = '';
    this.description = '';
    this.title = '';
  }



}

import { Injectable } from '@angular/core';
import { GitlabRestService } from './gitlab-rest.service';

@Injectable({
  providedIn: 'root'
})
export class GitlabServiceService {

  constructor(private rest: GitlabRestService) { }

  getIssues(): any {
    return this.rest.getIssues();
  }

  creatIssue(username: string, description: string, title: string): any {
    this.rest.getMemberByName(username).subscribe(arg => {
      if (arg[0]) {
        this.rest.CreateIssue(title, description, arg[0].id).subscribe();
      }
    });
  }

  getIssue(id: number): any {
    return this.rest.getIssueById(id);
  }

  deleteIssue(iid: number): any {
   return this.rest.deleteIssue(iid);
  }

  searchIssues(search: string): any {
    return this.rest.searchIssues(search);
  }

  getBoards(): any {
    return this.rest.getBoards();
  }

  updateIssue(issue_iid: number, username: string, description: string, title: string): any {
    this.rest.getMemberByName(username).subscribe(arg => {
      if (arg[0]) {
        console.log(title);
        this.rest.updateIssue(issue_iid, description, title, arg[0].id).subscribe();
      }
    });
  }
  closeIssue(issue_iid: number): any {
    return this.rest.closeIssue(issue_iid);
  }
}

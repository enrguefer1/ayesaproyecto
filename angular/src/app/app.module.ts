import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './dashboard/header/header.component';
import { FooterComponent } from './dashboard/footer/footer.component';
import { ListadoIssueComponent } from './dashboard/listado-issue/listado-issue.component';
import { DetalleIssueComponent } from './dashboard/listado-issue/detalle-issue/detalle-issue.component';
import { NuevoIssueComponent } from './dashboard/listado-issue/nuevo-issue/nuevo-issue.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { HeaderInterceptor } from './addHeader.interceptor';
import { NgDragDropModule } from 'ng-drag-drop';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    ListadoIssueComponent,
    DetalleIssueComponent,
    NuevoIssueComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgDragDropModule.forRoot()
  ],
  providers: [{provide: HTTP_INTERCEPTORS,
    useClass: HeaderInterceptor,
    multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }

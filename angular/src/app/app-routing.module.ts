import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoIssueComponent } from './dashboard/listado-issue/listado-issue.component';
import { NuevoIssueComponent } from './dashboard/listado-issue/nuevo-issue/nuevo-issue.component';
import { DetalleIssueComponent } from './dashboard/listado-issue/detalle-issue/detalle-issue.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [

{path: '', component: DashboardComponent},
{path: 'lista', component: ListadoIssueComponent},
{path: 'nuevo', component: NuevoIssueComponent},
{path: 'detalle', component: DetalleIssueComponent},
{ path: 'issue/:id', component: DetalleIssueComponent},
{ path: 'editIssue/:id', component: DetalleIssueComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
